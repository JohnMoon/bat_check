extern crate notify_rust;
use notify_rust::Notification;
use notify_rust::NotificationUrgency;

use std::fs::File;
use std::io::prelude::*;

static CHARGE_NOW: &str =  "/sys/class/power_supply/BAT0/charge_now";
static CHARGE_FULL: &str = "/sys/class/power_supply/BAT0/charge_full";
static STATUS: &str = "/sys/class/power_supply/BAT0/status";
static CHARGING_STR: &str = "Charging";

const CHARGE_WARN: f32 = 15.0;
const CHARGE_CRIT: f32 = 5.0;
const NOTIFICATION_TIMEOUT: i32 = 4000;

fn main() {

    /* If we're charging, just exit */
    if get_str_from_file(STATUS).trim() == CHARGING_STR {
        std::process::exit(0);
    }

    let charge_level: f32 = (get_float_from_file(CHARGE_NOW) /
                             get_float_from_file(CHARGE_FULL)) * 100.0;

    if charge_level < CHARGE_WARN {
        if Notification::new()
            .summary("Battery level low!")
            .body(&format!("Battery level: {0:.1$}%", charge_level, 2))
            .icon("dialog-information")
            .urgency(
                if charge_level < CHARGE_CRIT { 
                    NotificationUrgency::Critical
                } else { 
                    NotificationUrgency::Normal
                }
            )
            .timeout(NOTIFICATION_TIMEOUT)
            .show()
            .is_ok() {
                std::process::exit(0);
            } else {
                println!("error generating notification");
                std::process::exit(1);
            }
    }
}

fn get_str_from_file(file_path: &str) -> String {
    let mut file = File::open(file_path)
        .expect("file not found");

    let mut ret = String::new();
    file.read_to_string(&mut ret)
        .expect("failed to read file");

    ret
}

fn get_float_from_file(file_path: &str) -> f32 {
    get_str_from_file(file_path).trim().parse::<f32>()
        .unwrap_or_else(|_| panic!("failed to read float from file \"{}\"",
                                   file_path))
}
